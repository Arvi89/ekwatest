<?php

namespace App\Tests\Service;

use App\Service\EkwaService;
use App\Service\Ekwateur\EkwaSdkMock;
use App\Service\Ekwateur\Exception\EkwaException;
use PHPUnit\Framework\TestCase;

class EkwaServiceTest extends TestCase
{
    public function testValidatePromoCodeWrongCode(): void
    {
        $this->expectException(EkwaException::class);
        $ekwaService = new EkwaService(new EkwaSdkMock());
        $ekwaService->validatePromoCode("WRONG");
    }

    public function testValidatePromoCodeExpiredCode(): void
    {
        $this->expectException(EkwaException::class);
        $ekwaService = new EkwaService(new EkwaSdkMock());
        $ekwaService->validatePromoCode("WOODY_WOODPECKER");
    }

    public function testValidatePromoCodeValidCode(): void
    {
        $ekwaService = new EkwaService(new EkwaSdkMock());
        $result = $ekwaService->validatePromoCode("WOODY");
        $this->assertIsArray($result['compatibleOfferList']);
        $this->assertArrayHasKey("promoCode", $result);
    }
}
