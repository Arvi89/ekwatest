<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ValidateCodeCommandTest extends KernelTestCase
{
    public function testExecuteWrongCode(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'promoCode' => 'WRONG',
            // We don't want to write the json file in our test in case of success
            'dry' => '1',
        ));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('The promo code was not valid: The promo code does not exist.', $output);
    }

    public function testExecuteValidCode(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'promoCode' => 'BUZZ',
            'dry' => '1',
        ));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Json file written there', $output);
    }

    public function testExecuteValidCodeWithNewPath(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'promoCode' => 'BUZZ',
            'dry' => '1',
            'output' => 'new/path',
        ));

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('new/path', $output);
    }
}
