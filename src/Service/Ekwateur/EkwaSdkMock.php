<?php

namespace App\Service\Ekwateur;

use App\Service\Ekwateur\Entities\Offer;
use App\Service\Ekwateur\Entities\Promo;
use App\Service\Ekwateur\Exception\EkwaException;

class EkwaSdkMock implements EkwaSdk
{
    public function getAllOffers(): array
    {
        return array(
            0 =>
                array(
                    'offerType' => 'GAS',
                    'offerName' => 'EKWAG2000',
                    'offerDescription' => 'Une offre incroyable',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'ALL_2000',
                        ),
                ),
            1 =>
                array(
                    'offerType' => 'GAS',
                    'offerName' => 'EKWAG3000',
                    'offerDescription' => 'Une offre croustillante',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'GAZZZZZZZZY',
                        ),
                ),
            2 =>
                array(
                    'offerType' => 'ELECTRICITY',
                    'offerName' => 'EKWAE2000',
                    'offerDescription' => 'Une offre du tonnerre',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'ALL_2000',
                            2 => 'ELEC_IS_THE_NEW_GAS',
                        ),
                ),
            3 =>
                array(
                    'offerType' => 'ELECTRICITY',
                    'offerName' => 'EKWAE3000',
                    'offerDescription' => 'Pile l\'offre qu\'il vous faut',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'ELEC_IS_THE_NEW_GAS',
                            2 => 'BUZZ',
                            3 => 'ELEC_N_WOOD',
                        ),
                ),
            4 =>
                array(
                    'offerType' => 'WOOD',
                    'offerName' => 'EKWAW2000',
                    'offerDescription' => 'Une offre du envoie du bois',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'ALL_2000',
                            2 => 'WOODY',
                            3 => 'WOODY_WOODPECKER',
                        ),
                ),
            5 =>
                array(
                    'offerType' => 'WOOD',
                    'offerName' => 'EKWAW3000',
                    'offerDescription' => 'Une offre souscrite = un arbre planté',
                    'validPromoCodeList' =>
                        array(
                            0 => 'EKWA_WELCOME',
                            1 => 'WOODY',
                            2 => 'WOODY_WOODPECKER',
                            3 => 'ELEC_N_WOOD',
                        ),
                ),
        );
    }

    public function getOffer(string $offerName): Offer
    {
        $offers = $this->getAllOffers();
        foreach ($offers as $offer) {
            if ($offerName === $offer['offerName']) {
                return Offer::fromApi($offer);
            }
        }

        throw new EkwaException("The offer does not exist.");
    }

    public function getOffersFromPromo(string $promoCode): array
    {
        $return = array();
        $offers = $this->getAllOffers();
        foreach ($offers as $offer) {
            if (\in_array($promoCode, $offer['validPromoCodeList'], true)) {
                $return[] = Offer::fromApi($offer);
            }
        }

        return $return;
    }

    public function getAllPromos(): array
    {
        return array(
            0 =>
                array(
                    'code' => 'EKWA_WELCOME',
                    'discountValue' => 2,
                    'endDate' => '2019-10-04',
                ),
            1 =>
                array(
                    'code' => 'ELEC_N_WOOD',
                    'discountValue' => 1.5,
                    'endDate' => '2022-06-20',
                ),
            2 =>
                array(
                    'code' => 'ALL_2000',
                    'discountValue' => 2.75,
                    'endDate' => '2023-03-05',
                ),
            3 =>
                array(
                    'code' => 'GAZZZZZZZZY',
                    'discountValue' => 2.25,
                    'endDate' => '2018-08-02',
                ),
            4 =>
                array(
                    'code' => 'ELEC_IS_THE_NEW_GAS',
                    'discountValue' => 3.5,
                    'endDate' => '2022-04-13',
                ),
            5 =>
                array(
                    'code' => 'BUZZ',
                    'discountValue' => 2.75,
                    'endDate' => '2022-02-02',
                ),
            6 =>
                array(
                    'code' => 'WOODY',
                    'discountValue' => 1.75,
                    'endDate' => '2022-05-29',
                ),
            7 =>
                array(
                    'code' => 'WOODY_WOODPECKER',
                    'discountValue' => 1.15,
                    'endDate' => '2017-04-07',
                ),
        );
    }

    public function getPromo(string $promoCode): Promo
    {
        $promos = $this->getAllPromos();
        foreach ($promos as $promo) {
            if ($promoCode === $promo['code']) {
                return Promo::fromApi($promo);
            }
        }

        throw new EkwaException("The promo code does not exist.");
    }
}