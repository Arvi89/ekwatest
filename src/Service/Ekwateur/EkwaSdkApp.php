<?php

namespace App\Service\Ekwateur;

use App\Service\Ekwateur\Entities\Offer;
use App\Service\Ekwateur\Entities\Promo;
use App\Service\Ekwateur\Exception\EkwaException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class EkwaSdkApp implements EkwaSdk
{
    private string $baseUri;
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient, string $ekwaBaseUri)
    {
        $this->httpClient = $httpClient;
        $this->baseUri = $ekwaBaseUri;
    }

    /**
     * @param string $url
     * @param array $options
     * @return ResponseInterface
     * @throws EkwaException
     * @throws TransportExceptionInterface
     */
    private function get(string $url, array $options = array()): ResponseInterface
    {
        $result = $this->httpClient->request(
            'GET',
            $this->baseUri . $url,
            $options,
        );

        if (200 !== $result->getStatusCode()) {
            throw new EkwaException("Could not retrieve offers list.");
        }

        return $result;
    }

    /**
     * @return array
     * @throws EkwaException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAllOffers(): array
    {
        $offers = array();

        $result = $this->get('offerList');

        foreach ($result->toArray() as $arrayOffer) {
            $offer = Offer::fromApi($arrayOffer);
            $offers[] = $offer;
        }

        return $offers;
    }

    /**
     * @param string $offerName
     * @return Offer
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws EkwaException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getOffer(string $offerName): Offer
    {
        $result = $this->get('offerList', array(
            'query' => ['offerName' => $offerName],
        ));

        if (empty($result->toArray())) {
            throw new EkwaException("The offer does not exist.");
        }

        //There will only be one result, as names won't be the same for different offers
        return Offer::fromApi($result->toArray()[0]);
    }

    /**
     * @param string $promoCode
     * @return array
     * @throws EkwaException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getOffersFromPromo(string $promoCode): array
    {
        $offers = array();

        $result = $this->get('offerList', array(
            'query' => ['validPromoCodeList' => $promoCode],
        ));

        foreach ($result->toArray() as $arrayOffer) {
            $offer = Offer::fromApi($arrayOffer);
            $offers[] = $offer;
        }

        return $offers;
    }

    /**
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws EkwaException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAllPromos(): array
    {
        $promos = array();

        $result = $this->get('promoCodeList');

        foreach ($result->toArray() as $arrayOffer) {
            $offer = Offer::fromApi($arrayOffer);
            $promos[] = $offer;
        }

        return $promos;
    }

    /**
     * @param string $promoCode
     * @return Promo
     * @throws EkwaException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getPromo(string $promoCode): Promo
    {
        $result = $this->get('promoCodeList', array(
            'query' => ['code' => $promoCode],
        ));

        if (empty($result->toArray())) {
            throw new EkwaException("The promo code does not exist.");
        }

        //There will only be one result, as codes won't be the same for different promos
        return Promo::fromApi($result->toArray()[0]);
    }
}