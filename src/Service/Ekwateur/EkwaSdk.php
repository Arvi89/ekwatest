<?php

namespace App\Service\Ekwateur;

use App\Service\Ekwateur\Entities\Offer;
use App\Service\Ekwateur\Entities\Promo;

Interface EkwaSdk
{
    /**
     * @return array|Offer[]
     */
    public function getAllOffers(): array;

    /**
     * @param string $offerName
     * @return Offer
     */
    public function getOffer(string $offerName): Offer;

    /**
     * @param string $promoCode
     * @return array|Offer[]
     */
    public function getOffersFromPromo(string $promoCode): array;

    /**
     * @return array|Promo[]
     */
    public function getAllPromos(): array;

    /**
     * @param string $promoCode
     * @return Promo
     */
    public function getPromo(string $promoCode): Promo;
}