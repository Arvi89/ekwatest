<?php

namespace App\Service\Ekwateur\Entities;

use App\Service\Ekwateur\Exception\EkwaException;

class Promo
{
    private string $code;
    private float $discountValue;
    private \DateTime $endDate;

    private const SCHEMA = array(
        "code",
        "discountValue",
        "endDate",
    );

    /**
     * @param array $arrayOffer
     * @return Promo
     * @throws EkwaException
     */
    public static function fromApi(array $arrayOffer): Promo
    {
        $promo = new self();
        if (!empty(\array_diff_key(\array_flip(self::SCHEMA), $arrayOffer))) {
            throw new EkwaException("Error while creating Promo from the API");
        }
        $promo->setCode($arrayOffer['code'])
            ->setDiscountValue($arrayOffer['discountValue'])
            ->setEndDate(new \DateTime($arrayOffer['endDate']));

        return $promo;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Promo
     */
    public function setCode(string $code): Promo
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountValue(): float
    {
        return $this->discountValue;
    }

    /**
     * @param float $discountValue
     * @return Promo
     */
    public function setDiscountValue(float $discountValue): Promo
    {
        $this->discountValue = $discountValue;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Promo
     */
    public function setEndDate(\DateTime $endDate): Promo
    {
        $this->endDate = $endDate;
        return $this;
    }
}