<?php

namespace App\Service\Ekwateur\Entities;

use App\Service\Ekwateur\Exception\EkwaException;

class Offer
{
    public const ELECTRICITY = "ELECTRICITY";
    public const WOOD = "WOOD";
    public const GAS = "GAS";
    public const TYPES = array(self::ELECTRICITY, self::WOOD, self::GAS);

    private const SCHEMA = array(
        "offerName",
        "offerType",
        "validPromoCodeList",
        "offerDescription",
    );

    private string $type;
    private string $name;
    private string $description;
    private array $promos;

    /**
     * @param array $arrayOffer
     * @return Offer
     * @throws EkwaException
     */
    public static function fromApi(array $arrayOffer): Offer
    {
        $offer = new self();
        if (!empty(\array_diff_key(\array_flip(self::SCHEMA), $arrayOffer))) {
            throw new EkwaException("Error while creating Offer from the API");
        }
        $offer->setName($arrayOffer['offerName'])
            ->setType($arrayOffer['offerType'])
            ->setPromos($arrayOffer['validPromoCodeList'])
            ->setDescription($arrayOffer['offerDescription']);

        return $offer;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Offer
     */
    public function setType(string $type): Offer
    {
        if (\in_array($type, self::TYPES)) {
            $this->type = $type;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Offer
     */
    public function setName(string $name): Offer
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Offer
     */
    public function setDescription(string $description): Offer
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getPromos(): array
    {
        return $this->promos;
    }

    /**
     * @param array $promos
     * @return Offer
     */
    public function setPromos(array $promos): Offer
    {
        $this->promos = $promos;
        return $this;
    }

    public function hasPromo(string $promoName): bool
    {
        if (\in_array($promoName, $this->promos, true)) {
            return true;
        }

        return false;
    }
}