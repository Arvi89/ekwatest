<?php

namespace App\Service;

use App\Service\Ekwateur\EkwaSdk;
use App\Service\Ekwateur\Exception\EkwaException;

class EkwaService
{
    private EkwaSdk $ekwaSdk;

    public function __construct(EkwaSdk $ekwaSdk)
    {
        $this->ekwaSdk = $ekwaSdk;
    }

    /**
     * @param string $promoCode
     * @return array
     * @throws EkwaException
     */
    public function validatePromoCode(string $promoCode): array
    {
        $promo = $this->ekwaSdk->getPromo($promoCode);
        $now = new \DateTime();

        if ($promo->getEndDate() < $now) {
            throw new EkwaException("The promo code is expired.");
        }

        $output = array(
            'promoCode' => $promoCode,
            'endDate' => $promo->getEndDate()->format('Y-m-d'),
            'discountValue' => $promo->getDiscountValue(),
            'compatibleOfferList' => array(),
        );

        $offers = $this->ekwaSdk->getOffersFromPromo($promoCode);

        if (empty($offers)) {
            throw new EkwaException("There are no offers linked to that promo code.");
        }

        foreach ($offers as $offer) {
            $output['compatibleOfferList'][] = array(
                'name' => $offer->getName(),
                'type' => $offer->getType(),
            );
        }

        return $output;
    }
}