<?php

namespace App\Command;

use App\Service\EkwaService;
use App\Service\Ekwateur\Exception\EkwaException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class ValidateCodeCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';
    private EkwaService $ekwaService;
    private LoggerInterface $logger;
    private Filesystem $filesystem;
    private string $outputPath;

    public function __construct(EkwaService $ekwaService, LoggerInterface $logger, Filesystem $filesystem, string $outputPath, string $name = null)
    {
        $this->ekwaService = $ekwaService;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->outputPath = $outputPath;

        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Validates promo codes.')
            ->setHelp('This command verifies if a promo code is valid by checking if it exists, if it\'s linked to
            an offer and if it\'s not expired.')
            ->addArgument('promoCode', InputArgument::REQUIRED, 'Promo code to verify.')
            ->addArgument('dry', InputArgument::OPTIONAL, 'if set to 1, it won\'t write the json file in the end.')
            ->addArgument('output', InputArgument::OPTIONAL, 'Output directory where to write the json file.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $promoCode = $input->getArgument('promoCode');
            $jsonOutput = $this->ekwaService->validatePromoCode($promoCode);

            $filename = \bin2hex(\random_bytes(16));
            if (null !== $outputPath = $input->getArgument('output')) {
                $this->outputPath = $outputPath;
            }
            $path = $this->outputPath . '/' . $filename . '.json';

            $dry = $input->getArgument('dry');
            if (!($dry !== null && $dry === '1')) {
                $this->filesystem->dumpFile($path, \json_encode($jsonOutput, JSON_THROW_ON_ERROR));
            }

            $output->writeln("<info>Json file written there: $path</info>");
        } catch (EkwaException $ekwaException) {
            $output->write("<error>The promo code was not valid: ");
            $output->writeln($ekwaException->getMessage() . '</error>');
            $this->logger->debug("promo code not valid", array("message" => $ekwaException->getMessage()));

            return Command::FAILURE;
        } catch (\Throwable $throwable) {
            $output->writeln("<error>Something went wrong, please contact the administrator.</error>" . $throwable->getMessage());
            $this->logger->error("error while verifying promo code", array("message" => $throwable->getMessage()));

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}